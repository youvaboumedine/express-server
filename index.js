const express = require("express");
const timeout = require("connect-timeout");
const app = express();
const port = 3000;

const server = app.listen(port, () => {
  console.log(`Listening on port: ${port}`);
});

app.use(timeout("1s"));
app.get("/", (req, res) => {
  res.send("This route works.");
});

app.post("/timeout", (req, res) => {});
